﻿// File:    CProductImg.java
// Author:  甘孝俭
// email:   ganxiaojian@hotmail.com 
// QQ:      154986287
// http://www.8088net.com
// 协议声明：本软件为开源系统，遵循国际开源组织协议。任何单位或个人可以使用或修改本软件源码，
//          可以用于作为非商业或商业用途，但由于使用本源码所引起的一切后果与作者无关。
//          未经作者许可，禁止任何企业或个人直接出售本源码或者把本软件作为独立的功能进行销售活动，
//          作者将保留追究责任的权利。
// Created: 2015/12/6 10:23:34
// Purpose: Definition of Class CProductImg

package com.ErpCoreModel.Store;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Framework.CValue;

public class CProductImg extends CBaseObject
{

    public CProductImg()
    {
        TbCode = "SP_ProductImg";
        ClassName = "com.ErpCoreModel.Store.CProductImg";

    }

    
        public UUID getSP_Product_id()
        {
            if (m_arrNewVal.containsKey("sp_product_id"))
                return m_arrNewVal.get("sp_product_id").GuidVal;
            else
                return Util.GetEmptyUUID();
        }
        public void setSP_Product_id(UUID value)
        {
            if (m_arrNewVal.containsKey("sp_product_id"))
                m_arrNewVal.get("sp_product_id").GuidVal = value;
            else
            {
                CValue val = new CValue();
                val.GuidVal = value;
                m_arrNewVal.put("sp_product_id", val);
            }
        }
        public String getUrl()
        {
            if (m_arrNewVal.containsKey("url"))
                return m_arrNewVal.get("url").StrVal;
            else
                return "";
        }
        public void setUrl(String value)
        {
            if (m_arrNewVal.containsKey("url"))
                m_arrNewVal.get("url").StrVal = value;
            else
            {
                CValue val = new CValue();
                val.StrVal = value;
                m_arrNewVal.put("url", val);
            } 
       }
}
