<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.ColumnType" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnEnumVal" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.Base.CCompany" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="com.ErpCoreModel.Workflow.CWorkflowDef" %>
<%@ page import="com.ErpCoreModel.Workflow.CActivesDefMgr" %>
<%@ page import="com.ErpCoreModel.Workflow.ActivesType" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
    
    
<%
CUser m_User = null;
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
m_User=(CUser)request.getSession().getAttribute("User");
if (!m_User.IsRole("管理员"))
{
	response.getWriter().print("没有管理员权限！");
	response.getWriter().close();
	return ;
}
CCompany m_Company;
String B_Company_id = request.getParameter("B_Company_id");
if (Global.IsNullParameter(B_Company_id))
    m_Company = Global.GetCtx(this.getServletContext()).getCompanyMgr().FindTopCompany();
else
    m_Company = (CCompany)Global.GetCtx(this.getServletContext()).getCompanyMgr().Find(Util.GetUUID(B_Company_id));

String wfid = request.getParameter("wfid");
if (Global.IsNullParameter(wfid))
{
    response.getWriter().close();
    return;
}
CWorkflowDef m_WorkflowDef = (CWorkflowDef)m_Company.getWorkflowDefMgr().Find(Util.GetUUID(wfid));
if (m_WorkflowDef == null) //可能是新建的
{
    if (request.getSession().getAttribute("AddWorkflowDef") == null)
    {
        response.getWriter().close();
        return;
    }
    m_WorkflowDef = (CWorkflowDef)request.getSession().getAttribute("AddWorkflowDef");
}
CActivesDefMgr m_ActivesDefMgr = (CActivesDefMgr)m_WorkflowDef.getActivesDefMgr();

CTable m_Table = m_ActivesDefMgr.getTable();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        
        $(function ()
        {
            $("#AType").ligerComboBox({
                onSelected: function (newvalue, text)
                {
                    if(text=="按用户")
                    {
                        $("#B_User_id").ligerComboBox().setEnabled();
                        $("#B_Role_id").ligerComboBox().setDisabled();
                    }
                    else
                    {
                        $("#B_User_id").ligerComboBox().setDisabled();
                        $("#B_Role_id").ligerComboBox().setEnabled();
                    }
                }
            });

        }); 


        function onSubmit() {
            $.post(
                'AddActivesDef.do',
                {
                <% 
                List<CBaseObject> lstCol = m_Table.getColumnMgr().GetList();
                for (CBaseObject obj : lstCol)
                {
                    CColumn col = (CColumn)obj;
                    if (col.getCode().equalsIgnoreCase("id"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Created"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Creator"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Updated"))
                        continue;
                    else if (col.getCode().equalsIgnoreCase("Updator"))
                        continue;
                   
                    
                    if (col.getColType() == ColumnType.bool_type)
                    {
                    %>
                    <%=col.getCode() %>: $("#<%=col.getCode() %>").attr("checked"),
                    <%}
                    else
                    { %>
                    <%=col.getCode() %>: $("#<%=col.getCode() %>").val(),
                    <%}
                  } %>
                    Action: 'PostData',
                    wfid:'<%=request.getParameter("wfid")%>',
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid.loadData();
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        } 
        function onCancel() {
            $.post(
                'AddActivesDef.do',
                {
                    Action: 'Cancel',
                    wfid:'<%=request.getParameter("wfid")%>',
                    B_Company_id:'<%=request.getParameter("B_Company_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid.loadData();
                         parent.$.ligerDialog.close();
                         return true;
                     }
                     else {
                         $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text');
        } 
    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1" >
    <div>
        <table cellpadding="0" cellspacing="0" class="l-table-edit" >
        <% 
            //List<CBaseObject> lstCol = m_Table.ColumnMgr.GetList();
            for (CBaseObject obj : lstCol)
            {
                CColumn col = (CColumn)obj;

                if (col.getCode().equalsIgnoreCase("id"))
                    continue;
                else if (col.getCode().equalsIgnoreCase("Created"))
                    continue;
                else if (col.getCode().equalsIgnoreCase("Creator"))
                    continue;
                else if (col.getCode().equalsIgnoreCase("Updated"))
                    continue;
                else if (col.getCode().equalsIgnoreCase("Updator"))
                    continue;

                //定制元素====
                if (col.getCode().equalsIgnoreCase("WType"))
                {
                    %><input name="<%=col.getCode() %>" type="hidden" id="<%=col.getCode() %>" value="<%=ActivesType.Middle.ordinal() %>"/><%
                    continue;
                }
                else if (col.getCode().equalsIgnoreCase("WF_WorkflowDef_id"))
                {
                    %><input name="<%=col.getCode() %>" type="hidden" id="<%=col.getCode() %>" value="<%=request.getParameter("wfid") %>"/><%
                    continue;
                }
                else if (col.getCode().equalsIgnoreCase("Idx"))
                {
                    %><input name="<%=col.getCode() %>" type="hidden" id="<%=col.getCode() %>" value="<%=m_ActivesDefMgr.NewIdx() %>"/><%
                    continue;
                }
                //======
        %>
            <tr>
                <td align="right" class="l-table-edit-td"><%=col.getName() %>:</td>
                <td align="left" class="l-table-edit-td">
                <%if (col.getColType() == ColumnType.string_type)
                  { %>
                <input name="<%=col.getCode() %>" type="text" id="<%=col.getCode() %>" ltype="text" />
                <%}
                  else if (col.getColType() == ColumnType.text_type)
                  { %>
                  <textarea cols="100" rows="4" class="l-textarea" id="<%=col.getCode() %>" style="width:400px" ></textarea>
                <%}
                  else if (col.getColType() == ColumnType.int_type)
                  { %>
                  <input name="<%=col.getCode() %>" type="text" id="<%=col.getCode() %>" ltype='spinner' ligerui="{type:'int'}" class="required" validate="{digits:true,min:1,max:100}" />
                  <%}
                  else if (col.getColType() == ColumnType.long_type)
                  { %>
                  <input name="<%=col.getCode() %>" type="text" id="<%=col.getCode() %>" ltype='spinner' ligerui="{type:'int'}" class="required" validate="{digits:true,min:1,max:100}" />
                  <%}
                  else if (col.getColType() == ColumnType.bool_type)
                  { %>
                  <input id="<%=col.getCode() %>" type="checkbox" name="<%=col.getCode() %>" />
                  <%}
                  else if (col.getColType() == ColumnType.numeric_type)
                  { %>
                  <input name="<%=col.getCode() %>" type="text" id="<%=col.getCode() %>" ltype="text" />
                  <%}
                  else if (col.getColType() == ColumnType.guid_type)
                  { %>
                  <input name="<%=col.getCode() %>" type="text" id="<%=col.getCode() %>" ltype="text" />
                  <%}
                  else if (col.getColType() == ColumnType.datetime_type)
                  { %>
                  <input name="<%=col.getCode() %>" type="text" id="<%=col.getCode() %>" ltype="date" validate="{required:true}" />
                  <%}
                  else if (col.getColType() == ColumnType.ref_type)
                  { %>
                  <select name="<%=col.getCode() %>" id="<%=col.getCode() %>" ltype="select">
                  <option value="">(空)</option>
                  <%CTable RefTable = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(col.getRefTable());
                    CBaseObjectMgr BaseObjectMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(RefTable.getCode(), Util.GetEmptyUUID());
                    if (BaseObjectMgr == null)
                    {
                        BaseObjectMgr = new CBaseObjectMgr();
                        BaseObjectMgr.TbCode = RefTable.getCode();
                        BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
                    }

                    CColumn RefCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefCol());
                    CColumn RefShowCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefShowCol());
                    List<CBaseObject> lstObjRef = BaseObjectMgr.GetList();
                    for (CBaseObject objRef : lstObjRef)
                    { %>
	                <option value="<%=objRef.GetColValue(RefCol) %>"><%=objRef.GetColValue(RefShowCol).toString()%></option>
	                <%} %>
                </select>
                  <%}
                  else if (col.getColType() == ColumnType.enum_type)
                  { %>
                  <select name="<%=col.getCode() %>" id="<%=col.getCode() %>" ltype="select">
                  <option value="">(空)</option>
                  <%//引用显示字段优先
                      if (!col.getRefShowCol().equals(Util.GetEmptyUUID()))
                      {
                          CTable RefTable = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(col.getRefTable());
                          CBaseObjectMgr BaseObjectMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(RefTable.getCode(), Util.GetEmptyUUID());
                          if (BaseObjectMgr == null)
                          {
                              BaseObjectMgr = new CBaseObjectMgr();
                              BaseObjectMgr.TbCode = RefTable.getCode();
                              BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
                          }

                          CColumn RefShowCol = (CColumn)RefTable.getColumnMgr().Find(col.getRefShowCol());
                          List<CBaseObject> lstObjRef = BaseObjectMgr.GetList();
                          for (CBaseObject objRef : lstObjRef)
                          { %>
	                        <option value="<%=objRef.GetColValue(RefShowCol) %>"><%=objRef.GetColValue(RefShowCol)%></option>
	                    <%}
                      }
                      else
                      {
                          List<CBaseObject> lstObjEV = col.getColumnEnumValMgr().GetList();
                          for (CBaseObject objEV : lstObjEV)
                          {
                              CColumnEnumVal cev = (CColumnEnumVal)objEV;
                         %>
                         <option value="<%=cev.getVal()%>"><%=cev.getVal()%></option>
                      <%}
                      } %>
                </select>
                  <%} %>
                </td>
                <td align="left"></td>
            </tr>
          <%} %> 
            
        </table>
    </div>
    </form>
</body>
</html>