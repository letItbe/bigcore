<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/mytaglib" prefix="cc"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.Framework.CColumnMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Base.CUser" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.Base.AccessType" %>
<%@ page import="com.ErpCoreWeb.Common.EditObject" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>


<%
if (request.getSession().getAttribute("User") == null)
{
    response.getWriter().close();
    return ;
}
CUser m_User = (CUser) request.getSession().getAttribute("User");

CTable m_Table = null;
CView m_View = null;
int m_iCurPage = 1;
int m_iCurPageSize = 30;

String vid = request.getParameter("vid");
if (Global.IsNullParameter(vid))
{
	response.getWriter().close();
}
m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(vid));
if (m_View==null)
{
	response.getWriter().close();
}
pageContext.setAttribute("View", m_View);

m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());
pageContext.setAttribute("Table", m_Table);

Map<UUID, AccessType> m_sortRestrictColumnAccessType = m_User.GetRestrictColumnAccessTypeList(m_Table);
pageContext.setAttribute("RestrictColumnAccessType", m_sortRestrictColumnAccessType);

int m_iUIColCount=2;
if (!Global.IsNullParameter(request.getParameter("UIColCount")))
	m_iUIColCount = Integer.valueOf(request.getParameter("UIColCount"));
pageContext.setAttribute("UIColCount", m_iUIColCount);

//外面传递的默认值
Map<String, String> m_sortDefVal = new HashMap<String, String>();
for (CBaseObject obj : m_Table.getColumnMgr().GetList())
{
	CColumn col = (CColumn)obj;
    if (!Global.IsNullParameter(request.getParameter(col.getCode())))
        m_sortDefVal.put(col.getCode(), request.getParameter(col.getCode()));
}
pageContext.setAttribute("DefVal", m_sortDefVal);

//隐藏字段
Map<String, String> m_sortHideColumn = new HashMap<String, String>();
String sHideCols=request.getParameter("HideCols");
if (!Global.IsNullParameter(sHideCols))
{
	String[] arr = sHideCols.split(",");
    for (String code : arr)
    {
        m_sortHideColumn.put(code, code);
    }
}
pageContext.setAttribute("HideCols", m_sortHideColumn);


String id = request.getParameter("id");
if (Global.IsNullParameter(id))
{
    response.getWriter().print("请选择记录！");
    response.getWriter().close();
}

UUID m_guidParentId = Util.GetEmptyUUID();
String ParentId = request.getParameter("ParentId");
if (!Global.IsNullParameter(ParentId))
    m_guidParentId =Util.GetUUID(ParentId);
CBaseObjectMgr m_BaseObjectMgr = Global.GetCtx(this.getServletContext()).FindBaseObjectMgrCache(m_Table.getCode(), m_guidParentId);
if (m_BaseObjectMgr == null)
{
    m_BaseObjectMgr = new CBaseObjectMgr();
    m_BaseObjectMgr.TbCode = m_Table.getCode();
    m_BaseObjectMgr.Ctx = Global.GetCtx(this.getServletContext());
    String sWhere = String.format(" id='%s'", id);
    m_BaseObjectMgr.GetList(sWhere);
}
CBaseObject m_BaseObject = m_BaseObjectMgr.Find(Util.GetUUID(id));
if (m_BaseObject == null)
{
	response.getWriter().print("请选择记录！");
    response.getWriter().close();
}
pageContext.setAttribute("BaseObject", m_BaseObject);

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="../lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
     <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/jquery/jquery.PrintArea.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
    <script src="../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        //打印
        function onPrint() {
            $("#content").printArea();
        }

    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body style="padding:10px">
    <form id="form1">
    <div><div style="overflow: auto; float: right"><input id="btPrint" name="btPrint" type="button" value=" 打印 " onclick="onPrint()" /></div></div>
    <div id="content">
        <uc1:ShowRecordCtrl  Table="${pageScope.Table }" 
         View="${pageScope.View }" 
         RestrictColumnAccessType="${pageScope.RestrictColumnAccessType }" 
         UIColCount="${pageScope.UIColCount }" 
         DefVal="${pageScope.DefVal }" 
         HideCols="${pageScope.HideCols }" 
         BaseObject="${pageScope.BaseObject }" />
    </div>
    </form>
</body>
</html>