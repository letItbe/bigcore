<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.ErpCoreWeb.Common.Global" %>
<%@ page import="com.ErpCoreModel.Framework.Util" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObject" %>
<%@ page import="com.ErpCoreModel.Framework.CBaseObjectMgr" %>
<%@ page import="com.ErpCoreModel.Framework.CTable" %>
<%@ page import="com.ErpCoreModel.Framework.CColumn" %>
<%@ page import="com.ErpCoreModel.UI.CView" %>
<%@ page import="com.ErpCoreModel.UI.CViewCatalog" %>
<%@ page import="com.ErpCoreModel.UI.CViewDetail" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.UUID" %>
    
<%
if (request.getSession().getAttribute("User") == null)
{
    response.sendRedirect("../Login.jsp");
    return ;
}
CView m_View=null;
CTable m_Table =null;
CTable m_DetailTable =null;
CViewDetail m_ViewDetail =null;
String id = request.getParameter("id");
boolean m_bIsNew=false;
if (!Global.IsNullParameter(id))
{
    m_View = (CView)Global.GetCtx(this.getServletContext()).getViewMgr().Find(Util.GetUUID(id));
}
else
{
    m_bIsNew = true;
    if (request.getSession().getAttribute("NewMasterDetailView") == null)
    {
    	response.sendRedirect("MasterDetailViewInfo1.jsp?id=" + request.getParameter("id") + "&catalog_id=" + request.getParameter("catalog_id"));
		return ;
    }
    else
    {
    	Map<UUID, CView> sortObj = (Map<UUID, CView>)request.getSession().getAttribute("NewMasterDetailView");
        m_View =(CView) sortObj.values().toArray()[0];
    }
}
m_Table = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_View.getFW_Table_id());
m_ViewDetail = (CViewDetail)m_View.getViewDetailMgr().GetFirstObj();
if (m_ViewDetail != null)
	m_DetailTable = (CTable)Global.GetCtx(this.getServletContext()).getTableMgr().Find(m_ViewDetail.getFW_Table_id());

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script> 
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function() {
            $("#toptoolbar").ligerToolBar({ items: [
                { text: '删除', click: onDelete, icon: 'delete' }
            ]
            });
        });

        function onDelete() {
            var row = grid.getSelectedRow();
            if (row == null) {
                $.ligerDialog.alert('请选择行!');
                return;
            }
            
            $.post(
            'MasterDetailViewInfo4.do',
            {
                Action: 'Delete',
                id: '<%=request.getParameter("id") %>',
                catalog_id: '<%=request.getParameter("catalog_id") %>',
                delid: row.id
            },
             function(data) {
                 if (data == "" || data == null) {
                     $.ligerDialog.close();
                     grid.loadData(true);
                     return true;
                 }
                 else {
                     $.ligerDialog.warn(data);
                     return false;
                 }
             },
            'text');
        }

        function btPrev_onclick() {
            document.location.href = 'MasterDetailViewInfo3.jsp?id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %> ';
        }

        function btFinish_onclick() {
            
            //提交
            $.post(
                'MasterDetailViewInfo4.do',
                {
                    Action: 'PostData',
                    id: '<%=request.getParameter("id") %>',
                    catalog_id: '<%=request.getParameter("catalog_id") %>'
                },
                 function(data) {
                     if (data == "" || data == null) {
                         parent.grid.loadData(true);
                         parent.$.ligerDialog.close();
                    	 return true;
                     }
                     else {
                      	 $.ligerDialog.warn(data);
                         return false;
                     }
                 },
                 'text'
                 );
        }

        function btCancel_onclick() {
            $.post(
                'MasterDetailViewInfo4.do',
                {
                    id: '<%=request.getParameter("id") %>',
                    catalog_id: '<%=request.getParameter("catalog_id") %>',
                    Action: 'Cancel'
                },
                 function(data) {
                    if (data == "" || data == null) {
                        parent.grid.loadData(true);
                        parent.$.ligerDialog.close();
                       return true;
                    }
                    else {
                   		$.ligerDialog.warn(data);
                        return false;
                    }
                 },
                 'text');
        }

        function btAdd_onclick() {
            $.post(
                'MasterDetailViewInfo4.do',
                {
                    id: '<%=request.getParameter("id") %>',
                    catalog_id: '<%=request.getParameter("catalog_id") %>',
                    cbAndOr: $("#cbAndOr").val(),
                    cbColumn: $("#cbColumn").val(),
                    cbSign: $("#cbSign").val(),
                    txtVal: $("#txtVal").val(),
                    Action: 'Add'
                },
                 function(data) {
                    if (data == "" || data == null) {
                        parent.grid.loadData(true);
                        parent.$.ligerDialog.close();
                        $("#txtVal").val("");
                   	 return true;
                    }
                    else {
                     	$.ligerDialog.warn(data);
                        return false;
                    }
                 },
                 'text');
        }
    </script>
    <style type="text/css">
    #menu1,.l-menu-shadow{top:30px; left:50px;}
    #menu1{  width:200px;}
    </style>
    
    <script type="text/javascript">
        var grid;
        $(function ()
        {
            grid = $("#gridTable").ligerGrid({
            columns: [
                { display: '与或', name: 'AndOr' },
                { display: '字段', name: 'Column' },
                { display: '符号', name: 'Sign' },
                { display: '值', name: 'Val' }
                ],
                url: 'MasterDetailViewInfo4.do?Action=GetData&id=<%=request.getParameter("id") %>&catalog_id=<%=request.getParameter("catalog_id") %>',
                dataAction: 'server',
                usePager: false,
                width: '100%', height: '80%',
                onSelectRow: function (data, rowindex, rowobj)
                {
                    //$.ligerDialog.alert('1选择的是' + data.id);
                }
            });
        });
        
    </script>
</head>
<body style="padding:6px; overflow:hidden;"> 
    <div>过滤条件：</div>
    <div>主表：<input id="txtTbName" name="txtTbName" value="<%=m_Table.getName() %>" disabled="disabled" /></div>
  <div id="toptoolbar"></div> 
   <div id="gridTable" style="margin:0; padding:0"></div>
<form id="form1" >
<div>
    <select id="cbAndOr" name="cbAndOr">
    	<option value="and">与</option>
    	<option value="or">或</option>
    </select>
    <select id="cbColumn" name="cbColumn"> 
    <%
    List<CBaseObject> lstObj = m_Table.getColumnMgr().GetList();
    for (CBaseObject obj : lstObj)
    {
        CColumn col = (CColumn)obj;
        %>
        <option value="<%=col.getId().toString()%>"><%=col.getName()%></option>
        <% 
    }
    %>
    </select>
    <select id="cbSign" name="cbSign">
    	<option value="&gt;">&gt;</option>
    	<option value="&lt;">&lt;</option>
    	<option value="&gt;=">&gt;=</option>
    	<option value="&lt;=">&lt;=</option>
    	<option value="!=">!=</option>
    	<option value="like">like</option>
    </select>
    <input id="txtVal" type="text" /> 
    <input id="btAdd" type="button" value="添加" style="width:67px" onclick="return btAdd_Click()" />
</div>
<div>
    <input id="btPrev" type="button" value="上一步" style="width:60px" onclick="return btPrev_onclick()" />&nbsp;&nbsp;&nbsp;
    <input id="btFinish" type="button" value="完成" style="width:60px" onclick="return btFinish_onclick()" />&nbsp;&nbsp;&nbsp;&nbsp;
    <input id="btCancel" type="button" value="取消" style="width:60px" onclick="return btCancel_onclick()" />
</div>
</form>
</body>
</html>