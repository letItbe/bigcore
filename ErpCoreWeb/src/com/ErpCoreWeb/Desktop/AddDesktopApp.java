package com.ErpCoreWeb.Desktop;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CDesktopApp;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class AddDesktopApp
 */
@WebServlet("/AddDesktopApp")
public class AddDesktopApp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;
       
	public CUser m_User = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddDesktopApp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.request=request;
		this.response=response;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		
		if (Global.IsNullParameter(request.getParameter("Name")))
        {
            response.getWriter().print("名称不能空！");
            return;
        }
        if (Global.IsNullParameter(request.getParameter("Url")))
        {
        	response.getWriter().print("Url不能空！");
            return;
        }
        CUser user = (CUser)request.getSession().getAttribute("User");

        CDesktopApp BaseObject = new CDesktopApp();
        BaseObject.Ctx = Global.GetCtx(request.getSession().getAttribute("TopCompany").toString(),this.getServletContext());
        BaseObject.setName( request.getParameter("Name"));
        BaseObject.setUrl( request.getParameter("Url"));
        BaseObject.setB_User_id( user.getId());
        if(!Util.IsNullOrEmpty( request.getParameter("Icon")))
            BaseObject.setIconUrl( request.getParameter("Icon"));
        String w=request.getParameter("OpenwinWidth");
        String h=request.getParameter("OpenwinHeight");
        BaseObject.setOpenwinWidth( Integer.parseInt(w));
        BaseObject.setOpenwinHeight( Integer.parseInt(h));
        BaseObject.setCreator (user.getId());

        String GroupId = request.getParameter("GroupId");
        if (!Util.IsNullOrEmpty(GroupId))
            BaseObject.setUI_DesktopGroup_id( UUID.fromString(GroupId));

        if (!user.getDesktopAppMgr().AddNew(BaseObject, true))
        {
        	response.getWriter().print("添加失败！");
            return;
        }
	}

}
