package com.ErpCoreWeb.Desktop;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CContext;
import com.ErpCoreModel.Base.CUser;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.UI.CDesktop;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class Desktop
 */
@WebServlet("/Desktop")
public class Desktop extends HttpServlet {
	private static final long serialVersionUID = 1L;

	HttpServletRequest request;
	HttpServletResponse response;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Desktop() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		this.request=request;
		this.response=response;

		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

        String Action = request.getParameter("Action");
        if (Action == null) Action = "";


        if (Action.equalsIgnoreCase("DelDesktopApp"))
        {
            DelDesktopApp();
            return ;
        }
        else if (Action.equalsIgnoreCase("SetBackImg"))
        {
            SetBackImg();
            return ;
        }
        else if (Action.equalsIgnoreCase("UpdateOnlineState"))
        {
            UpdateOnlineState();
            return ;
        }
	}

    void DelDesktopApp()
    {
        String delid = request.getParameter("delid");
        if (Util.IsNullOrEmpty(delid))
            return;
        CUser user = (CUser)request.getSession().getAttribute("User");
        if (!user.getDesktopAppMgr().Delete(UUID.fromString(delid), true))
        {
            try {
				response.getWriter().print("删除应用失败！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    void SetBackImg()
    {
        String sBackImg = request.getParameter("BackImg");
        if (Util.IsNullOrEmpty(sBackImg))
            return;
        CUser user = (CUser)request.getSession().getAttribute("User");
        CDesktop desktop = (CDesktop)user.getDesktopMgr().GetFirstObj();
        if (desktop == null)
        {
            desktop = new CDesktop();
            desktop.Ctx = Global.GetCtx(this.getServletContext());
            desktop.setB_User_id ( user.getId());
            desktop.setCreator ( user.getId());
            user.getDesktopMgr().AddNew(desktop);
        }
        else
        {
            desktop.setUpdator( user.getId());
            user.getDesktopMgr().Update(desktop);
        }
        desktop.setBackImg( sBackImg);
        if (!user.getDesktopMgr().Save(true))
        {
        	try {
        		response.getWriter().print(Global.GetCtx(this.getServletContext()).LastError);
				//response.getWriter().print("保存失败！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return;
        }

    }

    //更新用户在线状态
    public void UpdateOnlineState()
    {
        CUser user = (CUser)request.getSession().getAttribute("User");
        if(user==null)
        	return ;
        user.UpdateOnlineTime();
        request.getSession().setAttribute("User", user);
    }
}
