

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ErpCoreModel.Base.CCompany;
import com.ErpCoreModel.Framework.CBaseObject;
import com.ErpCoreModel.Framework.Util;
import com.ErpCoreModel.Report.CReportCatalog;
import com.ErpCoreModel.UI.CDesktopGroup;
import com.ErpCoreModel.UI.CDesktopGroupMgr;
import com.ErpCoreModel.UI.CViewCatalog;
import com.ErpCoreModel.UI.CViewCatalogMgr;
import com.ErpCoreModel.Workflow.CWorkflowCatalog;
import com.ErpCoreWeb.Common.Global;

/**
 * Servlet implementation class AdminForm
 */
@WebServlet("/AdminForm")
public class AdminForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	HttpServletRequest request;
	HttpServletResponse response;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminForm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProc(request,response);
	}
	void doProc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.request=request;
		this.response=response;
		
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");

        String Action = request.getParameter("Action");
        if (Action == null) Action = "";

        if (Action.equalsIgnoreCase("GetWorkflowCatalog"))
        {
            GetWorkflowCatalog();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetWorkflowCatalogName"))
        {
            GetWorkflowCatalogName();
            return ;
        }
        else if (Action.equalsIgnoreCase("DelWorkflowCatalog"))
        {
            DelWorkflowCatalog();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetReportCatalog"))
        {
            GetReportCatalog();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetReportCatalogName"))
        {
            GetReportCatalogName();
            return ;
        }
        else if (Action.equalsIgnoreCase("DelReportCatalog"))
        {
            DelReportCatalog();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetReportCompany"))
        {
            GetReportCompany();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetWorkflowCompany"))
        {
            GetWorkflowCompany();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetViewCatalog"))
        {
            GetViewCatalog();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetViewCatalogName"))
        {
            GetViewCatalogName();
            return ;
        }
        else if (Action.equalsIgnoreCase("DelViewCatalog"))
        {
            DelViewCatalog();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetDesktopGroup"))
        {
            GetDesktopGroup();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetDesktopGroupName"))
        {
            GetDesktopGroupName();
            return ;
        }
        else if (Action.equalsIgnoreCase("DelDesktopGroup"))
        {
            DelDesktopGroup();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetSecurityCompany"))
        {
            GetSecurityCompany();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetSecurityCatalog"))
        {
            GetSecurityCatalog();
            return ;
        }
        else if (Action.equalsIgnoreCase("GetSecurityAccess"))
        {
            GetSecurityAccess();
            return ;
        }
	}

    // 工作流
    //获取工作流目录
    void GetWorkflowCatalog()
    {
        String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;
        String pid = request.getParameter("pid");
        UUID Parent_id = Util.GetEmptyUUID();
        if (pid!=null && pid.length()>0)
            Parent_id = UUID.fromString(pid);
        //context.Response.Write(@"[{text: '工作流'}]");
        String sJson = "[";
        List<CBaseObject> lstWorkflowCatalog = Company.getWorkflowCatalogMgr().GetList();
        for (int i=0;i< lstWorkflowCatalog.size();i++)
        {
            CWorkflowCatalog catalog = (CWorkflowCatalog)lstWorkflowCatalog.get(i);
            if (catalog.getParent_id().equals(Parent_id))
            {
                String sItem = String.format("{ isexpand: \"false\", name: \"nodeWorkflowCatalog\",\"id\":\"%s\",\"text\": \"%s\",\"url\": \"Workflow/WorkflowDefPanel.jsp?catalog_id=%s&B_Company_id=%s\",\"B_Company_id\":\"%s\", children: [] },",
                    catalog.getId().toString(),
                    catalog.getName(),
                    catalog.getId().toString(),
                    B_Company_id.toString(),
                    B_Company_id.toString());
                sJson += sItem;
            }
        }
        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    //获取工作流目录名
    void GetWorkflowCatalogName()
    {
    	String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;
        String id = request.getParameter("id");
        UUID guid = Util.GetEmptyUUID();
        if (id!=null && id.length()>0)
        	guid = UUID.fromString(id);
        else
            return;

        CWorkflowCatalog catalog = (CWorkflowCatalog)Company.getWorkflowCatalogMgr().Find(guid);
        if(catalog!=null)
			try {
				response.getWriter().println(catalog.getName());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }

    //删除工作流目录
    void DelWorkflowCatalog()
    {
    	String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;
        String delid = request.getParameter("delid");
        if (delid==null || delid.length()==0)
        {
            try {
				response.getWriter().println("请选择目录！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return;
        }
        UUID guid = UUID.fromString(delid);

        Company.getWorkflowCatalogMgr().Delete(guid);
        if (!Company.getWorkflowCatalogMgr().Save(true))  
        {
        	try {
				response.getWriter().println("删除失败！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    //获取工作流的单位目录
    void GetWorkflowCompany()
    {
        String sJson = "[";
        List<CBaseObject> lstObj = Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().GetList();
        for (int i=0;i< lstObj.size();i++)
        {
            CCompany Company = (CCompany)lstObj.get(i);

            String sItem = String.format("{ isexpand: \"false\", name: \"nodeWorkflowCompany\",\"id\":\"%s\",\"text\": \"%s\",\"url\": \"Workflow/WorkflowDefPanel.jsp?B_Company_id=%s\", children: [] },",
                Company.getId().toString(),
                Company.getName(),
                Company.getId().toString());
            sJson += sItem;
        }
        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }



    // 报表
    //获取报表目录
    void GetReportCatalog()
    {
    	String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;
        String pid = request.getParameter("pid");
        UUID Parent_id = Util.GetEmptyUUID();
        if (pid!=null && pid.length()>0)
            Parent_id = UUID.fromString(pid);
        //context.Response.Write(@"[{text: 'Report'}]");
        String sJson = "[";
        List<CBaseObject> lstReportCatalog = Company.getReportCatalogMgr().GetList();
        for (int i=0;i< lstReportCatalog.size();i++)
        {
            CReportCatalog catalog = (CReportCatalog)lstReportCatalog.get(i);
            if (catalog.getParent_id().equals(Parent_id))
            {
                String sItem = String.format("{ isexpand: \"false\", name: \"nodeReportCatalog\",\"id\":\"%s\",\"text\": \"%s\",\"url\": \"Report/ReportPanel.jsp?catalog_id=%s&B_Company_id=%s\",\"B_Company_id\":\"%s\", children: [] },",
                    catalog.getId().toString(),
                    catalog.getName(),
                    catalog.getId().toString(),
                    B_Company_id.toString(),
                    B_Company_id.toString());
                sJson += sItem;
            }
        }
        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }


    //获取报表目录名
    void GetReportCatalogName()
    {
    	String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;
        String id = request.getParameter("id");
        UUID guid = Util.GetEmptyUUID();
        if (id!=null && id.length()>0)
        	guid = UUID.fromString(id);
        else
            return;

        CReportCatalog catalog = (CReportCatalog)Company.getReportCatalogMgr().Find(guid);
        if (catalog != null)
			try {
				response.getWriter().println(catalog.getName());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }

    //删除报表目录
    void DelReportCatalog()
    {
    	String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;
        String delid = request.getParameter("delid");
        if (delid==null || delid.length()==0)
        {
            try {
				response.getWriter().println("请选择目录！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return;
        }
        UUID guid = UUID.fromString(delid);

        Company.getReportCatalogMgr().Delete(guid);
        if (!Company.getReportCatalogMgr().Save(true))
        {
        	try {
				response.getWriter().println("删除失败！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    //获取报表的单位目录
    void GetReportCompany()
    {
        String sJson = "[";
        List<CBaseObject> lstObj = Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().GetList();
        for (int i=0;i< lstObj.size();i++)
        {
            CCompany Company = (CCompany)lstObj.get(i);

            String sItem = String.format("{ isexpand: \"false\", name: \"nodeReportCompany\",\"id\":\"%s\",\"text\": \"%s\",\"url\": \"Report/ReportPanel.jsp?B_Company_id=%s\", children: [] },",
                Company.getId().toString(),
                Company.getName(),
                Company.getId().toString());
            sJson += sItem;
        }
        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }



    // 视图
    //获取视图目录
    void GetViewCatalog()
    {
    	String pid = request.getParameter("pid");
        UUID Parent_id = Util.GetEmptyUUID();
        if (pid!=null && pid.length()>0)
            Parent_id = UUID.fromString(pid);
        //context.Response.Write(@"[{text: 'Report'}]");
        String sJson = "[";
        List<CBaseObject> lstViewCatalog = Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getViewCatalogMgr().GetList();
        for (int i=0;i< lstViewCatalog.size();i++)
        {
            CViewCatalog catalog = (CViewCatalog)lstViewCatalog.get(i);
            if (catalog.getParent_id().equals(Parent_id))
            {
                String sItem = String.format("{ isexpand: \"false\", name: \"nodeViewCatalog\",\"id\":\"%s\",\"text\": \"%s\",\"url\": \"View/ViewPanel.jsp?catalog_id=%s\", children: [] },",
                    catalog.getId().toString(),
                    catalog.getName(),
                    catalog.getId().toString());
                sJson += sItem;
            }
        }
        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    //获取视图目录名
    void GetViewCatalogName()
    {
    	String id = request.getParameter("id");
        UUID guid = Util.GetEmptyUUID();
        if (id!=null && id.length()>0)
        	guid = UUID.fromString(id);
        else
            return;

        CViewCatalog catalog = (CViewCatalog)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getViewCatalogMgr().Find(guid);
        if (catalog != null)
			try {
				response.getWriter().println(catalog.getName());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    //删除视图目录
    void DelViewCatalog()
    {
    	String delid = request.getParameter("delid");
        if (delid==null || delid.length()==0)
        {
            try {
				response.getWriter().println("请选择目录！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return;
        }
        UUID guid = UUID.fromString(delid);

        CViewCatalogMgr ViewCatalogMgr = Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getViewCatalogMgr();
        ViewCatalogMgr.Delete(guid);
        if (!ViewCatalogMgr.Save(true))
        {
        	try {
				response.getWriter().println("删除失败！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    // 桌面组
    //获取桌面组
    void GetDesktopGroup()
    {
        String sJson = "[";
        List<CBaseObject> lstDesktopGroupCatalog = Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getDesktopGroupMgr().GetList();
        for (int i=0;i< lstDesktopGroupCatalog.size();i++)
        {
            CDesktopGroup group = (CDesktopGroup)lstDesktopGroupCatalog.get(i);

            String sItem = String.format("{ isexpand: \"false\", name: \"nodeDesktopGroup\",\"id\":\"%s\",\"text\": \"%s\",\"url\": \"\", children: [] },",
                group.getId().toString(),
                group.getName());
            sJson += sItem;
        }
        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    //获取桌面组名
    void GetDesktopGroupName()
    {
    	String id = request.getParameter("id");
        UUID guid = Util.GetEmptyUUID();
        if (id!=null && id.length()>0)
        	guid = UUID.fromString(id);
        else
            return;

        CDesktopGroup group = (CDesktopGroup)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getViewCatalogMgr().Find(guid);
        if (group != null)
			try {
				response.getWriter().println(group.getName());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    //删除桌面组
    void DelDesktopGroup()
    {
    	String delid = request.getParameter("delid");
        if (delid==null || delid.length()==0)
        {
            try {
				response.getWriter().println("请选择目录！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return;
        }
        UUID guid = UUID.fromString(delid);

        CDesktopGroupMgr DesktopGroupMgr = Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getDesktopGroupMgr();
        DesktopGroupMgr.Delete(guid);
        if (!DesktopGroupMgr.Save(true))
        {
        	try {
				response.getWriter().println("删除失败！");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    // 安全性
    //获取安全性的单位目录
    void GetSecurityCompany()
    {
        String sJson = "[";
        List<CBaseObject> lstObj = Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().GetList();
        for (int i=0;i< lstObj.size();i++)
        {
            CCompany Company = (CCompany)lstObj.get(i);

            String sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityCompany\",\"id\":\"%s\",\"text\": \"%s\", children: [] },",
                Company.getId().toString(),
                Company.getName());
            sJson += sItem;
        }
        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    //获取安全性目录
    void GetSecurityCatalog()
    {
    	String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;

        String sJson = "[";

        String sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityUser\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/User/UserPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "用户",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityOrg\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Org/OrgPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "组织",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityRole\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Role/RolePanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "角色",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityAccess\",\"id\":\"\",\"text\": \"%s\",\"B_Company_id\":\"%s\",children: [] },",
            "权限",
            B_Company_id.toString());
        sJson += sItem;

        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    //获取权限目录
    void GetSecurityAccess()
    {
    	String B_Company_id = request.getParameter("B_Company_id");
        if (B_Company_id==null || B_Company_id.length()==0)
            return;
        CCompany Company = (CCompany)Global.GetCtx((String)request.getSession().getAttribute("TopCompany"),this.getServletContext()).getCompanyMgr().Find(UUID.fromString(B_Company_id));
        if (Company == null)
            return;

        String sJson = "[";

        String sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityDesktopGroupAccess\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Access/DesktopGroupAccessPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "桌面组权限",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityViewAccess\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Access/ViewAccessPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "视图权限",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityTableAccess\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Access/TableAccessPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "表权限",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityColumnAccess\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Access/ColumnAccessPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "字段权限",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityMenuAccess\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Access/MenuAccessPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "菜单权限",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;
        sItem = String.format("{ isexpand: \"false\", name: \"nodeSecurityReportAccess\",\"id\":\"\",\"text\": \"%s\",\"url\": \"Security/Access/ReportAccessPanel.jsp?B_Company_id=%s\",\"B_Company_id\":\"%s\" },",
            "报表权限",
            B_Company_id.toString(),
            B_Company_id.toString());
        sJson += sItem;

        if(sJson.endsWith(","))
        	sJson = sJson.substring(0, sJson.length()-1);
        sJson += "]";
        try {
			response.getWriter().println(sJson);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
